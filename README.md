# e8-is-public

express middleware to configure public paths

## config file sample

```javascript
module.exports = [
  /\/auth\/.+/,
  /\/image\/.+/
]
```


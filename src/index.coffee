module.exports = ( paths ) -> ( req, res, next ) ->

  for p in paths
    if p.test( req.url )
      req.isPublic = true
      return next()

  next()

